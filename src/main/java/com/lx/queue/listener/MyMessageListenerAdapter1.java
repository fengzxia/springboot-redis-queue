package com.lx.queue.listener;

import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * 消息监听1
 *
 * @author 段誉
 * @create 2019-03-28 14:55
 */
public class MyMessageListenerAdapter1 extends MessageListenerAdapter {
  public MyMessageListenerAdapter1(Object delegate, String defaultListenerMethod) {
    super(delegate, defaultListenerMethod);
  }
}

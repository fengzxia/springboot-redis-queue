package com.lx.queue;

import org.springframework.stereotype.Component;

/**
 * 消息处理接收到的方法
 *
 * @author 段誉
 * @create 2019-03-28 14:34
 */
@Component
public class MessageReceiver {

  /**
   * 接收消息方法
   */
  public void receiverMessage1(String message) {
    System.out.println("收到一条chat1新消息：" + message);
  }

  public void receiverMessage2(String message) {
    System.out.println("收到一条chat2新消息：" + message);
  }
}

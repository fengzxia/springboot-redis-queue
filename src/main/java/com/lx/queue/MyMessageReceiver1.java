package com.lx.queue;

import org.springframework.stereotype.Component;

/**
 * @author 段誉
 * @create 2019-03-28 14:57
 */
@Component
public class MyMessageReceiver1 {
  public void receiverMessage(String message) {
    System.out.println("MyMessageReceiver1接收到消息：" + message);
  }
}

package com.lx.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 消息发送者
 *
 * @author 段誉
 * @create 2019-03-28 14:31
 */
//开启定时器功能
//@EnableScheduling
@Component
public class MessageSender2 {
  @Autowired
  private StringRedisTemplate stringRedisTemplate;

  /**
   * 间隔2秒，通过stringRedisTemplate对象向redis消息队列chat频道发布消息
   */
//  @Scheduled(fixedDelay = 2000)
  public void sendMessage() {
    stringRedisTemplate.convertAndSend("chat1", "来自chat1的消息" + Math.random());
  }
}
